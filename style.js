$(document).ready(function () {
    let liste = new Array();

    const proxy = "https://cors-anywhere.herokuapp.com/"
    const test = "http://62.210.247.201:9000/test"
  
    $.get(proxy + test, function (data) {
     
        for (i = 0; i < data.length; i++) {
            for (j = 0; j < data[i].weight; j++) {
                liste.push(data[i].image)
            }
        }
        shuffle(liste)
        for (l = 0; l<liste.length; l++){
            var img = liste[l];
            $(".carousel-inner").prepend("<div class='carousel-item'><img class='d-block w-100' src='" + img + "'></div>");
        }
        $('.carousel-item:first-child').addClass('active');

    });
});

function shuffle(array) {
    array.sort(() => Math.random() - 0.5);
}

